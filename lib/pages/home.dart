import 'package:flutter/material.dart';
import 'package:tanihub_clone/widgets/icon_badge.dart';
import 'package:carousel_pro/carousel_pro.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}
final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
final SnackBar snackBar = const SnackBar(content: Text('Showing Snackbar'));

class _HomeState extends State<Home> {
  final TextEditingController _searchControl = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.menu,
          ),
          onPressed: (){},
        ),
//        title: TextFormField(
//            decoration: InputDecoration(
//              contentPadding: EdgeInsets.all(10.0),
//              filled: true,
//              fillColor: Colors.white,
//              border: OutlineInputBorder(
//                borderRadius: BorderRadius.circular(5.0),
//                borderSide: BorderSide(color: Colors.white,),
//              ),
//              enabledBorder: OutlineInputBorder(
//                borderSide: BorderSide(color: Colors.white,),
//                borderRadius: BorderRadius.circular(5.0),
//              ),
//              hintText: "Cari Buah, Sayur, Daging...",
//              focusedBorder: OutlineInputBorder(
//                  borderSide: BorderSide(color: Colors.white, width: 20.0),
//                  borderRadius: BorderRadius.circular(20.0)),
//            )
//        ),

        actions: <Widget>[

          IconButton(
            icon: IconBadge(
              icon: Icons.notifications_none,
            ),
            onPressed: (){},
          ),
        ],
      ),
      
      body: ListView(
        children: <Widget>[
          // Akun(),
          // Divider(),
          Padding(
            padding: EdgeInsets.all(5),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.blueGrey[50],
                borderRadius: BorderRadius.all(
                  Radius.circular(15.0),
                ),
              ),
              child: TextField(
                style: TextStyle(
                  fontSize: 12.0,
                  color: Colors.blueGrey[300],
                ),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(0.0),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide(color: Colors.white,),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white,
                    ),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  hintText: "Lokasi",
                  prefixIcon: Icon(
                    Icons.location_on,
                    color: Colors.blueGrey[300],
                  ),
                  hintStyle: TextStyle(
                    fontSize: 15.0,
                    color: Colors.blueGrey[300],
                  ),
                ),
                maxLines: 1,
                controller: _searchControl,
              ),
            ),
          ),
          SizedBox(
            height: 25,
          ),
          SizedBox(
              height: 200.0,
              width: 350.0,
              child: Carousel(
                images: [
                  NetworkImage('http://149.28.16.248/banner01.png'),
                  NetworkImage('http://149.28.16.248/bannernews.png'),
                  NetworkImage('http://149.28.16.248/bannertopup.png'),
                  // ExactAssetImage("assets/images/LaunchImage.jpg")
                ],
                dotSize: 4.0,
                dotSpacing: 15.0,
                dotColor: Colors.lightGreenAccent,
                indicatorBgPadding: 5.0,
                dotBgColor: Colors.white.withOpacity(0.5),
                borderRadius: true,
                moveIndicatorFromBottom: 180.0,
                noRadiusForIndicator: true,
              )
          ),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: EdgeInsets.all(4),
          
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                color: Colors.greenAccent[200],
                borderRadius: BorderRadius.all(
                  Radius.circular(7.0),
                ),
              ),
                  child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          RaisedButton.icon(
                            icon: Icon(Icons.confirmation_number),
                            label: Text('Voucher Saya'),
                            onPressed: (){},
                            color: Colors.greenAccent[200],
                            elevation: 0.0,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                          ),
                          Text('Mother Fucker', style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          ],
                      ),
                  ),
            ),
          ),
        ],
      ),
    );
  }
}

