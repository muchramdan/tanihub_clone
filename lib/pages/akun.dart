import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
class Akun extends StatefulWidget {
  @override
  _AkunState createState() => _AkunState();
}

class _AkunState extends State<Akun> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Akun Saya'),
//        actions: <Widget>[
//          IconButton(
//              icon: Icon(Icons.more_horiz),
//              onPressed: (){}),
//        ],
      ),
      body: ListView(
        children: <Widget>[
          mail(),
          Divider(),
          profile(),
          facebook(),
        ],
      ),
    );
  }
}

class mail extends StatelessWidget{
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: ListTile(
        title: Text('Much Ramdan', style: TextStyle(fontWeight: FontWeight.bold),
        ),
        subtitle: Row(children: <Widget>[
          Text('Muchramdna123@gmail.com', style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ],
        ),
      ),

    );
  }
}

class profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: ListTile(
        title: Text('Tanihun Voucher', style: TextStyle(fontWeight: FontWeight.bold),
        ),
        subtitle: Row(children: <Widget>[
          RaisedButton.icon(
            icon: Icon(Icons.confirmation_number),
            label: Text('3 Voucher'),
            onPressed: (){},
            color: Colors.grey[200],
            elevation: 0.0,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          ),
          Padding(padding: EdgeInsets.all(8.0),
          ),
        ],
        ),
      ),
    );
  }
}

class facebook extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: ListTile(
        title: Row(children: <Widget>[
          new SizedBox(
            width: 360,
            height: 35.0,
            child: FacebookSignInButton(onPressed: () {}
            ),
          )
        ],
        ),
      ),

    );
  }
}