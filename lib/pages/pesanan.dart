import 'package:flutter/material.dart';
import 'package:tanihub_clone/pesanan/saatini.dart' as saatini;
import 'package:tanihub_clone/pesanan/selesai.dart' as selesai;
import 'package:tanihub_clone/pesanan/dibatalkan.dart' as dibatalkan;

class Pesanan extends StatefulWidget {
  @override
  _PesananState createState() => _PesananState();
}

class _PesananState extends State<Pesanan> with SingleTickerProviderStateMixin{

  TabController controller;

  @override
  void initState() {
    controller =  new TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: new Text('Pesanan'),
          bottom: TabBar(
          controller: controller,
            tabs: <Widget>[
              new Tab(text: "Saat Ini"),
              new Tab(text: "Selesai"),
              new Tab(text: "Dibatalkan"),
            ],
          ),
          actions: <Widget>[
          IconButton(
              icon: Icon(Icons.rotate_right),
              onPressed: (){}),
        ],
      ),
      body: new TabBarView(
        controller: controller,
        children: <Widget>[
        new saatini.Saatini(),
        new selesai.Selesai(),
        new dibatalkan.Dibatalkan(),
      ],
      ),
    );
  }
}