import 'package:flutter/material.dart';

class Saatini extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: new Column(
          children: <Widget>[
            new Padding(padding: new EdgeInsets.all(80.0),),
            new Icon(Icons.shopping_basket, size: 35.0,),
            new Text("Belanja Dan dukung petani kita sekarang!", style: new TextStyle(fontSize: 16.0),),
            new Padding(padding: new EdgeInsets.all(4.0),),
            new Text("Dukung petani kira memilih produk lokal", style: new TextStyle(fontSize: 10.0),),
            new Text("Dalam keseharianmu bersama kami!", style: new TextStyle(fontSize: 10.0),),
            new Padding(padding: new EdgeInsets.all(4.0),),
            new RaisedButton(
              onPressed: () {},
              child: new Text(
                "Mulai Belanja"
              ),
            )
          ],
        ),
      ),

    );

  }
}
