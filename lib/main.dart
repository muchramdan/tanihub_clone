import 'package:flutter/material.dart';
import 'package:tanihub_clone/pages/home.dart';
import 'package:tanihub_clone/pages/pesanan.dart';
import 'package:tanihub_clone/pages/tagihan.dart';
import 'package:tanihub_clone/pages/akun.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
      
    
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'tanihub_clone'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

int _selectedIndex = 0;
final _layoutPage =[
  Home(),
  Pesanan(),
  Tagihan(),
  Akun(),
];

void _onTapItem(int index){
  setState(() {
    _selectedIndex = index;
  });
}

  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
    
      body: _layoutPage.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home')
             ),
             BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            title: Text('Pesanan')
             ),
              BottomNavigationBarItem(
            icon: Icon(Icons.receipt),
            title: Text('Tagihan')
             ),
             BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            title: Text('Akun')
             ),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        onTap: _onTapItem,
      ),
      
    );
  }
}
